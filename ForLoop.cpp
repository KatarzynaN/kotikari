#include "Program.h"
#include "Function.h"


void ForLoop::Compile(Function* fun){
	ofstream* output=program->output;
	int* position=0;
	int ref=0;
	if(!fun){
		ref=3;int savedPos=*position;
		position=program->memPointer;
	}else{
		ref=6;
		position=fun->memPointer;
	}
	
	counter->Compile(fun);
	
	int labelBegin=*(program->label);
	(*program->label)++;

	int labelBody=*(program->label);
	(*program->label)++;

	int labelEnd=*(program->label);
	(*program->label)++;

	(*output)<<"L "<<labelBegin<<":\n";
	int savedPos=*position;
	/*(*output)<<"        R7=R7-4;//Place for prev R4\n";
	(*output)<<"        P(R7)=R4;//Save prev R4\n";
	(*output)<<"        R7=R7-4;//Place for prev R7\n";
	(*output)<<"        P(R7)=R7;//Save prev R7\n";	
	(*output)<<"        R4=R7;\n";*/
	//(*position)+=8;

	int before=*position;
	int add=condition->Compile(fun);	
	int offset=*position-before;
	(*output)<<"        R5=P(R6-"<<add<<");\n";	
	(*output)<<"        IF(R5) GT("<<labelBody<<");\n";
	(*output)<<"        GT("<<labelEnd<<");\n";
	(*output)<<"L "<<labelBody<<":\n";
	
		

	
	

	block->Compile(fun);

	
	
	operation->Compile(fun);



/*	(*output)<<"        R7=P(R4);//Restore prev R7\n";
	(*output)<<"        R4=P(R7+4);//Restore prev R4\n";*/
	(*output)<<"        R7=R6-"<<savedPos<<";//Restore the pointer\n";

	(*position)=savedPos;

	(*output)<<"        GT("<<labelBegin<<");\n";

	(*output)<<"L "<<labelEnd<<":\n";
	//Unsafe
	(*position)+=offset;
}
