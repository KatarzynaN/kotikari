#include "Program.h"
#include "Function.h"

int FunctionCall::Compile(Function* fun){
	ofstream* output=program->output;
	int* position=0;
	if(!fun){
		position=program->memPointer;
	}else{
		position=fun->memPointer;
	}
	int beginPosition=*position;
	int ret=0;

	std::deque<int> addresses;
	for(int i=expressions->expressions.size()-1;i>=0;i--){
		Expression* ex=expressions->expressions[i];
		addresses.push_front(ex->Compile(fun));
	}
		
	//Result int initializaiton
	int resSize=function->result->varType.size*4;
	*position+=resSize;
	//function->result->address=-8;
	(*output)<<"        R7=R7-"<<resSize<<";//Set result address\n";
	(*output)<<"//Pos: "<<*position<<"\n";
	if(function->result->varType.type==0){
	//Result is int
		(*output)<<"        P(R7)=0;\n";
	}else{
	//Result is array
		int aSize=function->result->varType.size;
		printf("RSS %i %i",resSize,aSize);
		for(int i=0;i<aSize;i++){
			(*output)<<"        P(R7+"<<(4*i)<<")=0;\n";
		}
	}

	ret=*position;

	int add=function->label;
	int label=*(program->label);
	(*program->label)++;
	//Set label and R6
	(*output)<<"        R7=R7-8;//Set place for label and R6\n";
	(*output)<<"        P(R7+4)="<<label<<";\n";
	(*output)<<"        P(R7)=R6;\n";

	(*position)+=8;


	//Arguments
	
	for(int i=expressions->expressions.size()-1;i>=0;i--){
		Expression* ex=expressions->expressions[i];
		int exSize=ex->type.size*4;
		(*output)<<"        R7=R7-"<<exSize<<";//Set arg address\n";
	//	(*position)+=exSize;
		if(ex->type.type==0){
		//Is int
			(*output)<<"        R5=P(R"<<expressions->expressions[i]->ref<<"-"<<addresses[i]<<");\n";
			(*output)<<"        P(R7)=R5;\n";
		}else{
		//Is array
			for(int j=0;j<ex->type.size;j++){
				(*output)<<"        R5=P(R"<<expressions->expressions[i]->ref<<"-"<<addresses[i]-j*4<<");\n";
				(*output)<<"        P(R7+"<<j*4<<")=R5;\n";
			}
		}
	}



	(*output)<<"        GT("<<add<<");\n";
	(*output)<<"L "<<label<<":    //return from function call\n";
	
	return ret;
/*	if(strcmp("main",name)==0){
		label=1;
	}else{
		label=*(program->label);
		(*program->label)++;
	}
	(*output)<<"L "<<label<<":    //FunctionBegin "<<name<<" \n";
	(*output)<<"        R6=R7;\n";
	if(returnType.type==0){
		//Result int initializaiton
		*position+=4;
		result->address=*position;
		(*output)<<"        R7=R7-4;\n";
		(*output)<<"        P(R7)=0;\n";
	}else{
		//Result array initializaiton
	}

	//Add argument initialization

	instructions->Compile();
	*position=beginPosition;*/
}
