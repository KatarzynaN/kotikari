#pragma once
#include "ExpressionList.h"
class Function;
class ExpressionList;
class FunctionCall{
public:
	FunctionCall(){}
	~FunctionCall(){}
	int Compile(Function* fun);
	Function* function;
	ExpressionList* expressions;
	Program* program;
};
