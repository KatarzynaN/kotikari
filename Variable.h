#include "TableEntry.h"
#include <vector>
#pragma once
#ifndef VT_INT
#define VT_INT 0
#define VT_ARRAY 1
#endif

class VarType{
public:
	
	VarType(){}
	VarType(int t,int s){type=t;size=s;}
	~VarType(){}
	int type;
	int size;
	

};

class Variable: public TableEntry{
public:
	Variable(char* n,VarType var_t,bool isVar=true):TableEntry(n,isVar?TE_VAR:TE_CONST){varType=var_t;address=0;}
	~Variable(){}
	void Print(){if(varType.type==VT_INT)printf("Var Int %s\n",name);else printf("Var Array[%d] %s\n",varType.size,name);}

	
	VarType varType;
	int address;
	int ref;
	//char* name
};

/*
class Constant:public TableEntry{
public:
	Constant(char* n,VarType var_t):TableEntry(n,TE_CONST){varType=var_t;}
	~Constant(){}
	void Print(){if(varType.type==VT_INT)printf("Const Int %s\n",name);else printf("Const Array[%d] %s\n",varType.size,name);}
	VarType varType;
};*/
