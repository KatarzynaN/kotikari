#include "Program.h"
#include "IfCondition.h"
#include "Function.h"

void IfCondition::Compile(Function* fun){
	ofstream* output=program->output;
	int* position=0;
	int ref=0;
	if(!fun){
		ref=3;
		position=program->memPointer;
	}else{
		ref=6;
		position=fun->memPointer;
	}

	//Condition
	int add=ifExpression->Compile(fun);
	
	int labelIf=*(program->label);
	(*program->label)++;

	int labelEnd=*(program->label);
	(*program->label)++;
	

	//(*output)<<"        R7=R7-4;//Place for condition value\n";
	(*output)<<"        R5=P(R"<<ifExpression->ref<<"-"<<add<<");\n";
	//(*output)<<"        P(R7)=R5;\n";
	//(*position)+=4;	
	(*output)<<"        IF(R5) GT("<<labelIf<<");\n";
	//Place for Else
	int savedPos=0;
	if(hasElse){
		savedPos=*position;
		/*(*output)<<"        R7=R7-4;//Place for prev R4\n";
		(*output)<<"        P(R7)=R4;//Save prev R4\n";
		(*output)<<"        R7=R7-4;//Place for prev R7\n";
		(*output)<<"        P(R7)=R7;//Save prev R7\n";	
		(*output)<<"        R4=R7;\n";
		(*position)+=8;*/


		elsePart->Compile(fun);

		/*(*output)<<"        R7=P(R4);//Restore prev R7\n";
		(*output)<<"        R4=P(R7+4);//Restore prev R4\n";*/
		(*output)<<"        R7=R6-"<<savedPos<<";//Restore the pointer\n";

		(*position)=savedPos;
	}
	(*output)<<"        GT("<<labelEnd<<");\n";
	(*output)<<"L "<<labelIf<<":\n";
	//Place for IF
	savedPos=*position;
	/*(*output)<<"        R7=R7-4;//Place for prev R4\n";
	(*output)<<"        P(R7)=R4;//Save prev R4\n";
	(*output)<<"        R7=R7-4;//Place for prev R7\n";
	(*output)<<"        P(R7)=R7;//Save prev R7\n";	
	(*output)<<"        R4=R7;\n";
	(*position)+=8;*/


	ifPart->Compile(fun);

	/*(*output)<<"        R7=P(R4);//Restore prev R7\n";
	(*output)<<"        R4=P(R7+4);//Restore prev R4\n";*/
	(*output)<<"        R7=R6-"<<savedPos<<";//Restore the pointer\n";

	(*position)=savedPos;

	(*output)<<"L "<<labelEnd<<":\n";







	
}
