#pragma once
#include "Instruction.h"

class InstructionBlock{
public:
	InstructionBlock(){}
	~InstructionBlock(){}
	void Compile(Function* fun);
	std::deque<Instruction*> instructions;
	Program* program;
};
