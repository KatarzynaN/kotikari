#pragma once

class StringExpression{
public:
	StringExpression(){}
	~StringExpression(){}
	Expression* expression;
	char* text;
	bool isText;
	Program* program;
	void Compile(Function* fun);
};
