#include <iostream>
#include <cstdio>
#ifndef TE_MARKER
#define TE_MARKER 0
#define TE_CONST 1
#define TE_VAR 2
#define TE_FUNCTION 3
#endif
#pragma once

class TableEntry{
public:
	TableEntry(char* n,int t){name=n;type=t;}
	~TableEntry(){}
	virtual void Print(){printf("%d %s\n",type,name);}
	char* name;
	int type;
	
};
