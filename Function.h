#include "TableEntry.h"
#include "Variable.h"
#include "InstructionBlock.h"
#include "Program.h"
#include <list>
#include <vector>
#pragma once

class Function:public TableEntry{
public:
	Function(char*n,VarType rtype):TableEntry(n,TE_FUNCTION){returnType=rtype;memPointer=new int;*memPointer=0;}
	Function(char*n,VarType rtype,std::vector<Variable*> args):TableEntry(n,TE_FUNCTION){arguments=args;returnType=rtype;memPointer=new int;*memPointer=0;}
	~Function(){}
	int Compile();
	void AddArgument(Variable* arg){arguments.push_back(arg);}
	void Print(){if(returnType.type==VT_INT)printf("Function Int %s\n",name);else printf("Function Array[%d] %s\n",returnType.size,name);}
	
	VarType returnType;
//	char* name;
	std::vector<Variable*> arguments;
	InstructionBlock* instructions;
	Program* program;
	int label;
	Variable* result;
	int* memPointer;
};
