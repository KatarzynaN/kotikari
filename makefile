# Makefile: A simple makefile for ex1.
default:
	bison -d kotikariCompiler3.ypp
	flex kotikari2.l
	g++ -o kotik SemanticAnalyser.cpp  kotikariCompiler3.tab.cpp Program.cpp ArrayConst.cpp AssignmentEx.cpp BinaryEx.cpp DeclarationAssignment.cpp Expression.cpp ExpressionList.cpp ForLoop.cpp FunctionCall.cpp Function.cpp FunctionList.cpp IfCondition.cpp InstructionBlock.cpp Instruction.cpp PrintInstruction.cpp StringExpression.cpp UnaryEx.cpp VarBlock.cpp Variable.cpp VariableEx.cpp lex.yy.c -lfl -Wno-write-strings
