#include "Program.h"
#include "Function.h"
#include <string>
#include <cstring>

int Function::Compile(){
	ofstream* output=program->output;
	int* position=memPointer;
	int beginPosition=0;
	int ret=0;
	

	if(strcmp("main",name)==0){
		label=1;
	}else{
		label=*(program->label);
		(*program->label)++;
	}
	int argSize=0;
	for(int i=0;i<arguments.size();i++){
		
		argSize+=arguments[i]->varType.size*4;
	}
	(*output)<<"L "<<label<<":    //FunctionBegin "<<name<<" \n";
	(*output)<<"        R6=R7+"<<argSize<<";//pos: "<<*position<<"\n";
	(*position)+=argSize;
	beginPosition=*position;

//-------------------------------------------------------UNSAFE
	if(returnType.type==0){
		//Result int initializaiton
		*position+=4;
		result->address=*position;
		result->ref=6;
		ret = *position;
		(*output)<<"        R7=R7-4;\n";
		(*output)<<"        P(R7)=0;//pos: "<<*position<<"\n";
	}else{

		int aSize=returnType.size;
		*position+=(4*aSize);
		result->address=*position;
		result->ref=6;
		ret=*position;
		(*output)<<"        R7=R7-"<<(4*aSize)<<";\n";
		for(int i=0;i<aSize;i++){
			(*output)<<"        P(R7+"<<(4*i)<<")=0;\n";
		}
		//Result array initializaiton
	}
//-------------------------------------------------------UNSAFE


	//Add argument initialization
	argSize=0;

	for(int i=arguments.size()-1;i>=0;i--){
		Variable* var=arguments[i];
		argSize+=var->varType.size*4;
		var->address=argSize;
		var->ref=6;
	}
	
		(*output)<<"        //pos: "<<*position<<"\n";
	instructions->Compile(this);
	*position=beginPosition;


	if(strcmp("main",name)!=0){
//-------------------------------------------------------UNSAFE
		if(returnType.type==0){
			//Result int rewrite
			(*output)<<"        R5=P(R6-"<<result->address<<");\n";
			(*output)<<"        P(R6+8)=R5;\n";
		} else{
			//Result array rewrite
			int aSize=returnType.size;
			for(int i=0;i<aSize;i++){
				
				(*output)<<"        R5=P(R6-"<<(result->address-4*i)<<");\n";
				(*output)<<"        P(R6+"<<8+(4*i)<<")=R5;\n";
			}
		}
//-------------------------------------------------------UNSAFE
		(*output)<<"        //clean the mess\n";
		(*output)<<"        R7=R6;\n";
		(*output)<<"        R6=P(R6);\n";
		(*output)<<"        R5=P(R7+4);\n";
		(*output)<<"        GT(R5);\n";
	}
	return ret;
}
