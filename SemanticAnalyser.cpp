#include "SemanticAnalyser.h"

bool SemanticAnalyser::isIdentifierAllowed(char* n,int t){
	bool outside=false;
	for(std::list<TableEntry*>::reverse_iterator iter=table.rbegin();iter!=table.rend();++iter){
		TableEntry *current=*iter;
		//Are we still inside the context?
		if(current->type==TE_MARKER){
			outside=true;		
		}else{
			//Is name match detected?
			if(!strcmp(current->name,n)){
				if(t!=TE_VAR){
					//Only hiding by a local variable is allowed
					printf("Error: Duplicate identifier %s\n",n);
					return false;			
				}else{
					//Only hiding a constant or a variable from outside of the context is allowed
					if(!outside||current->type==TE_FUNCTION){
						printf("Error: Duplicate identifier %s\n",n);
						return false;
					}else{
						//It means that we found an identifier that can be hidden. No further search needed. Only one such identifier can exist
						return true;				
					}
				}	
			}
		}		
	}
	//Identifier not found. The name is legal
	return true;
}

int SemanticAnalyser::GetEntryType(char* name){
	for(std::list<TableEntry*>::reverse_iterator iter=table.rbegin();iter!=table.rend();++iter){
		TableEntry *current=*iter;
		if(strcmp(current->name,name)==0){
			return current->type;		
		}
	}
	return -1;
}

int SemanticAnalyser::MatchAssignTypes(char* n,Expression* ex){
	int et=GetEntryType(n);
	if(et==1){
		//A constant
		printf("\nError: CAN'T ASSIGN TO A CONSTANT!\n");
		return 1;	
	}
	if(et==3){
		//A function
		printf("\nError: CAN'T ASSIGN TO A FUNCTION!\n");
		return 2;	
	}
	if(ex->type.type!=GetValueType(n).type){
		//Type mismatch
		printf("\nError: TYPE MISMATCH!\n");
		return 3;	
	}
	if(ex->type.size!=GetValueType(n).size){
		//Size mismatch		
		printf("\nError: ARRAY SIZE MISMATCH!\n");
		return 4;
	}
	return 0;
}

int SemanticAnalyser::MatchExprType(VarType vt,Expression* ex){
	if(ex->type.type!=vt.type){
		//Type mismatch
		printf("\nError: TYPE MISMATCH!\n");
		return 3;	
	}
	if(ex->type.size!=vt.size){
		//Size mismatch		
		printf("\nError: ARRAY SIZE MISMATCH!\n");
		return 4;
	}
	return 0;
}

VarType SemanticAnalyser::GetValueType(char* name){
	for(std::list<TableEntry*>::reverse_iterator iter=table.rbegin();iter!=table.rend();++iter){
		TableEntry *current=*iter;
		if(strcmp(current->name,name)==0){
			if(current->type==1){
				Variable* ret=(Variable*)current;
				return ret->varType;			
			}
			if(current->type==2){
				Variable* ret=(Variable*)current;
				return ret->varType;			
			}
			if(current->type==3){
				Function* ret=(Function*)current;
				return ret->returnType;			
			}
					
		}
	}
	return VarType();
}

int SemanticAnalyser::CheckFunctionCall(char* n,ExpressionList* el){
	int et=GetEntryType(n);
	if(et!=3){

		printf("\nError: NOT A NUMBER!\n");
		return 5;	
	}
	std::vector<Variable*>* args=getArgList(n);
	
	if(el->size!=args->size()){
		printf("\nError: ARGUMENT NUMBER MISMATCH!\n");
		return 6;	
	}
	for(int i=0;i<(el->size);i++){
		printf("%d %d",args->at(i)->varType.type,el->typeVec[i].type);
		if(args->at(i)->varType.type!=el->typeVec[i].type){
		
			//Type mismatch
			printf("\nError: ARGUMENT TYPE MISMATCH!\n");
			return 3;		
		}
		if(args->at(i)->varType.size!=el->typeVec[i].size){
			printf("\nError: ARGUMENT ARRAY SIZE MISMATCH!\n");
			return 4;		
		}
	}
	return 0;
}

void SemanticAnalyser::PrintTable(){
printf("Table:\n");
for(std::list<TableEntry*>::iterator iter=table.begin();iter!=table.end();++iter){
		TableEntry *current=*iter;
		current->Print();
}
}

Variable* SemanticAnalyser::GetVariableByName(char* n){
	for(std::list<TableEntry*>::reverse_iterator iter=table.rbegin();iter!=table.rend();++iter){
		TableEntry *current=*iter;
		if(strcmp(current->name,n)==0){
			printf("Variable found %s\n",n);
			Variable* ret=(Variable*)current;
			return ret;					
		}
	}
	printf("Variable NOT found %s\n",n);
	return 0;
}

Function* SemanticAnalyser::GetFunctionByName(char* n){
	for(std::list<TableEntry*>::reverse_iterator iter=table.rbegin();iter!=table.rend();++iter){
		TableEntry *current=*iter;
		if(strcmp(current->name,n)==0){
			if(current->type==TE_FUNCTION){
				Function* ret=(Function*)current;
				return ret;	
			}				
		}
	}
	return 0;
}

void SemanticAnalyser::addToTable(TableEntry *entry){
	table.push_back(entry);
}

void SemanticAnalyser::addMarker(){
	TableEntry *entry=new TableEntry("#marker",TE_MARKER);
	addToTable(entry);
}

std::vector<Variable*>* SemanticAnalyser::getArgList(char* name){
	for(std::list<TableEntry*>::reverse_iterator iter=table.rbegin();iter!=table.rend();++iter){
		TableEntry *current=*iter;
		if(strcmp(current->name,name)==0){
			if(current->type==3){
				Function* ret=(Function*)current;
				return &(ret->arguments);			
			}		
		}
	}
	return 0;
}

Function* SemanticAnalyser::getLastFunction(){
	for(std::list<TableEntry*>::reverse_iterator iter=table.rbegin();iter!=table.rend();++iter){
		TableEntry *current=*iter;
		if(current->type==TE_FUNCTION){
			Function* ret=(Function*)current;
			return ret;		
		}
	}
	return 0;
}

void SemanticAnalyser::OpenBlock(){
	addMarker();
}

void SemanticAnalyser::CloseBlock(){
	eraseTillMarker();
}

void SemanticAnalyser::eraseTillMarker(){
	std::list<TableEntry*>::reverse_iterator iter=table.rbegin();
	while(iter!=table.rend()){
		TableEntry *current=*iter;
		if(current->type==TE_MARKER){
			++iter;
			iter= std::list<TableEntry*>::reverse_iterator(table.erase(iter.base()));
			return ;		
		}else{
			++iter;
			iter= std::list<TableEntry*>::reverse_iterator(table.erase(iter.base()));
		}
	}
}

bool SemanticAnalyser::AddIntVar(char* name){

	if(!isIdentifierAllowed(name,TE_VAR)){
		return false;
	}
	VarType type(VT_INT,1);
	Variable *v=new Variable(name,type);
	addToTable(v);
	return true;
}

bool SemanticAnalyser::AddArrayVar(char* name,int size){

	if(!isIdentifierAllowed(name,TE_VAR)){
		return false;
	}
	VarType type(VT_ARRAY,size);
	Variable *v=new Variable(name,type);
	addToTable(v);
	return true;
}

bool SemanticAnalyser::AddIntConst(char* name){
	if(!isIdentifierAllowed(name,TE_CONST)){
		return false;
	}
	VarType type(VT_INT,1);
	Variable *c=new Variable(name,type,false);
	addToTable(c);
	return true;
}

bool SemanticAnalyser::AddArrayConst(char* name,int size){
	if(!isIdentifierAllowed(name,TE_CONST)){
		return false;
	}
	VarType type(VT_ARRAY,size);
	Variable *c=new Variable(name,type,false);
	addToTable(c);
	return true;
}

bool SemanticAnalyser::AddIntFunction(char* name){
	if(!isIdentifierAllowed(name,TE_FUNCTION)){
		return false;
	}
	VarType type(VT_INT,1);
	Function *f=new Function(name,type);
	addToTable(f);
	addMarker();
	for(int i=0;i<pendingArgs.size();i++){
		f->AddArgument((pendingArgs[i]));
		addToTable(pendingArgs[i]);
	}
	pendingArgs.clear();
	Variable* ret=new Variable("result",type);
	f->result=ret;
	addToTable(ret);
	return true;
}

bool SemanticAnalyser::AddArrayFunction(char* name,int size){
	if(!isIdentifierAllowed(name,TE_FUNCTION)){
		return false;
	}
	VarType type(VT_ARRAY,size);
	Function *f=new Function(name,type);
	addToTable(f);
	addMarker();
	for(int i=0;i<pendingArgs.size();i++){
		f->AddArgument((pendingArgs[i]));
		addToTable(pendingArgs[i]);
	}
	pendingArgs.clear();
	Variable* ret=new Variable("result",type);
	f->result=ret;
	addToTable(ret);
	return true;
}

bool SemanticAnalyser::AddIntArgument(char* name){

	if(GetFunctionByName(name)){
		return false;
	}
	for(int i=0;i<pendingArgs.size();i++){
		if(strcmp(name,pendingArgs[i]->name)==0){
			printf("Duplicate identifier %s\n",name);
			return false;		
		}
	}
	VarType type(VT_INT,1);
	Variable *v=new Variable(name,type);
	
	//Function* fun=getLastFunction();
	//fun->AddArgument(*v);
	//addToTable(v);
	pendingArgs.push_back(v);
	return true;
}

bool SemanticAnalyser::AddArrayArgument(char* name,int size){
	if(!isIdentifierAllowed(name,TE_VAR)){
		return false;
	}
	for(int i=0;i<pendingArgs.size();i++){
		if(strcmp(name,pendingArgs[i]->name)==0){
			printf("Duplicate identifier %s\n",name);
			return false;		
		}
	}
	VarType type(VT_ARRAY,size);
	Variable *v=new Variable(name,type);
	
	//Function* fun=getLastFunction();
	//fun->AddArgument(*v);
	//addToTable(v);
	pendingArgs.push_back(v);
	return true;
}
