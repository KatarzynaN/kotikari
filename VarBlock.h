#pragma once
#include "Program.h"
#include "DeclarationAssignment.h"

class VarBlock{
public:
	VarBlock(){}
	~VarBlock(){}
	void Compile();

	std::deque<DeclarationAssignment*> declarations;
	Program* program;
};
