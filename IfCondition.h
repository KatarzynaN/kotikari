#pragma once
#include "Expression.h"
class InstructionBlock;

class IfCondition{
public:
	IfCondition(){}
	~IfCondition(){}
	void Compile(Function* fun);
	Expression* ifExpression;
	InstructionBlock* ifPart;
	InstructionBlock* elsePart;
	bool hasElse;
	Program* program;
};
