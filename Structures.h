//Ten plik jest nieaktualny. Wszystkie klasy znajduja sie w osobnych plikach!

class Program{
public:
	Program(){}
	~Program(){}
	SemanticAnalyser* analyser;
	VarBlock*	constBlock;
	VarBlock*	varBlock;
	FunctionList	functions;
};

class VarBlock{
public:
	std::vector<DeclarationAssignment> declarations;
	Program* program;
};

class DeclarationAssignment{
public:
	VarType type;
	char* name;
	Expression* expression;
	ArrayConst* arrayConst;
	bool isExpression;
	bool hasValue;
	Program* program;
};

class ArrayConst{
public:
	std::vector<Expression> expressions;
	Program* program;
};

class Expression{
public:
	Expression(){}
	~Expression(){}
	VarType type;

	int variant;
	
	int number;
	VariableEx* variable;
	FunctionCall* functionCall;
	AssignmentEx* assignment;
	UnaryEx* unaryE;
	BinaryEx* binaryE;
	Program* program;
	
};

class FunctionCall{
public:
	Function* function;
	std::vector<Expression> expressions;
};

class AssignmentEx{
public:
	VariableEx* variable;
	Expression* expression;
	ArrayConst* arrayConst;
	bool isExpression;
};

class UnaryEx{
public:
	int operatorNum; // 0-minus, 1-wykrzyknik
	Expression* expression;
	
};

class BinaryEx{
public:
	int operatorNum; //2-or,3-and,4-compl,5-compr,6-eq,7-plu,8-min,9-mul,10-div,11-mod
	Expression* expression1;
	Expression* expression2;
};

class VariableEx{
public:
	Variable* variable;
	int position;
	Program* program;
};

class FunctionList{
public:
	FunctionList(){}
	~FunctionList(){}
	std::vector<Function> functions;
	Program* program;
	
};

class Function:public TableEntry{
public:
	Function(char*n,VarType rtype):TableEntry(n,TE_FUNCTION){returnType=rtype;}
	Function(char*n,VarType rtype,std::vector<Variable> args):TableEntry(n,TE_FUNCTION){arguments=args;returnType=rtype;}
	~Function(){}
	void AddArgument(Variable arg){arguments.push_back(arg);}
	void Print(){if(returnType.type==VT_INT)printf("Function Int %s\n",name);else printf("Function Array[%d] %s\n",returnType.size,name);}
	
	VarType returnType;
//	char* name;
	std::vector<Variable> arguments;
	InstructionBlock* instructions;
	Program* program;
};

class InstructionBlock{
public:
	std::vector<Instruction> instructions;
	Program* program;
};

class Instruction{
public:
	int type;
	IfCondition* condition;
	ForLoop* forLoop;
	Expression* expression;
	DeclarationAssignment* declaration;
	PrintInstruction* printInstruction;
	Program* program;
	
};

class IfCondition{
public:
	Expression* ifExpression;
	InstructionBlock* ifPart;
	InstructionBlock* elsePart;
	bool hasElse;
	Program* program;
};

class ForLoop{
public:
	DeclarationAssignment* counter;
	Expression* condition;
	Expression* operation;
	InstructionBlock* block;
	Program* program;
};

class PrintInstruction{
public:
	std::vector<StringExpression> strings;
	Program* program;
};

class StringExpression{
public:
	Expression* expression;
	char* text;
	bool isText;
	Program* program;
};
