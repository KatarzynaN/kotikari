%{
#include "SemanticAnalyser.h"
#include "Program.h"
#include "kotikariCompiler2.tab.hpp"
#include <iostream>
using namespace std;
%}
DIG [0-9]
CHR [a-zA-Z_]
CHRDIG {DIG}|{CHR}
OPPLU [+]
OPMUL [*]
OPDIV [/]
OPMOD [%]
OPCOMPL [<]
OPCOMPM [>]
OPEQ [=]
OPAND [&]
OPOR [|]
OPCOL :
OPEXCL !
OPMIN -
OPPLPL \+\+
OPPRINT <\+\+
KCONST const
KVAR var
KIF if
KELSE else
KFOR for
INT int
TYPE {INT}
L_PARENT [(]
R_PARENT [)]
L_BRACE [{]
R_BRACE [}]
L_BRACKET [\[]
R_BRACKET [\]]
SEMICOLON [;]
COMMA [,]
QT \"
COM #
WS [ \t\n]
SIGN [^"]
TXT {SIGN}*

%%
{COM}.*\n      /*ignore*/
{QT}{TXT}*{QT} { printf("text(%s) ",yytext); yylval.str = strdup(yytext); return TXT; }
{KCONST}       { printf("const "); yylval.str = strdup(yytext); return CONST; }
{KVAR}         { printf("var "); yylval.str = strdup(yytext); return VAR; }
{KIF}          { printf("if "); yylval.str = strdup(yytext); return IF; }
{KELSE}        { printf("else "); yylval.str = strdup(yytext); return ELSE; }
{KFOR}         { printf("for "); yylval.str = strdup(yytext); return FOR; }
{TYPE}         { printf("type(%s) ",yytext); yylval.str = strdup(yytext); return TYP; }
{L_PARENT}     { printf("lpar\n"); yylval.sym = yytext[0]; return LPAR; }
{R_PARENT}     { printf("rpar\n"); yylval.sym = yytext[0]; return RPAR; }
{L_BRACE}      { printf("lbrace\n"); yylval.sym = yytext[0]; return LBRACE; }
{R_BRACE}      { printf("rbrace\n"); yylval.sym = yytext[0]; return RBRACE; }
{L_BRACKET}    { printf("lbracket "); yylval.sym = yytext[0]; return LBRACKET; }
{R_BRACKET}    { printf("rbracket "); yylval.sym = yytext[0]; return RBRACKET; }
{SEMICOLON}    { printf("semicolon\n"); yylval.sym = yytext[0]; return SEMICOL; }
{COMMA}        { printf("comma "); yylval.sym = yytext[0]; return COMMA; }
{OPPLU}        { printf("opplu "); yylval.sym = yytext[0]; return PLU; }
{OPMUL}        { printf("opmul "); yylval.sym = yytext[0]; return MUL; }
{OPDIV}        { printf("opdiv "); yylval.sym = yytext[0]; return DIV; }
{OPMOD}        { printf("opmod "); yylval.sym = yytext[0]; return MOD; }
{OPCOMPL}      { printf("opcompl "); yylval.sym = yytext[0]; return COMPL; }
{OPCOMPM}      { printf("opcompm "); yylval.sym = yytext[0]; return COMPM; }
{OPEQ}         { printf("opeq "); yylval.sym = yytext[0]; return EQ; }
{OPAND}        { printf("opand "); yylval.sym = yytext[0]; return AND; }
{OPOR}         { printf("opor "); yylval.sym = yytext[0]; return OR; }
{OPCOL}        { printf("opcol "); yylval.sym = yytext[0]; return COL; }
{OPEXCL}       { printf("opexcl "); yylval.sym = yytext[0]; return EXCL; }
{OPMIN}        { printf("opmin "); yylval.sym = yytext[0]; return MIN; }
{OPPLPL}       { printf("opplpl "); yylval.str = strdup(yytext); return PLPL; }
{OPPRINT}      { printf("opprint "); yylval.str = strdup(yytext); return PRINT; }
{DIG}+         { printf("numb(%s) ",yytext); yylval.val = atoi(yytext); return NUM; }
{CHR}{CHRDIG}* { printf("ident(%s) ",yytext); yylval.str = strdup(yytext); return IDN; }
{WS}+          /*ignore*/
<<EOF>>        { return 0; }
. 	       { printf("all "); yylval.sym = yytext[0]; return ALL; }

%%


