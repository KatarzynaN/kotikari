#include "Program.h"
#include "Function.h"

void Program::Compile(){
	printf("Compilation start\n\n\n");
	output=new ofstream();
	output->open("output.q.c");
	(*output)<<"#include \"Q.h\"\n";
	(*output)<<"BEGIN\n";
	(*output)<<"L 0:    R3=R7;\n"; //First free adress
	(*output)<<"        R6=R7;\n";
	
	(*output)<<"//ConstBlock Begin\n";
	constBlock->Compile();
	
	(*output)<<"//VarBlock Begin\n";
	varBlock->Compile();
	
	(*output)<<"        GT(1); \n";
	(*output)<<"//Functions Begin\n";
	functions->Compile();
	
	(*output)<<"//Program End\n";
	(*output)<<"L 2:    GT(-2); \n";
	(*output)<<"END\n";


	output->close();

	//Kompilacja Q code'a	

//	printf("%s\n",varBlock->declarations[0]->name);
//	printf("%s\n",functions->functions[0]->name);
//	printf("%s\n",functions->functions[0]->instructions->instructions[1]->declaration->name);


/*(*output)<<"        R7=R7-40;\n";
	(*output)<<"        R0 = 0; \n";
	(*output)<<"        R1 = 1;\n";
	(*output)<<"L 1:    R2 = 4 * R0; \n";
	(*output)<<"        I(R2+R7) = R1;\n";
	(*output)<<"        R0 = R0 + 1;  \n";
	(*output)<<"        R1 = R1 * R0; \n";
	(*output)<<"        IF (R0 < 10) GT(1); \n";
	(*output)<<"        GT(-1);\n";*/
}
