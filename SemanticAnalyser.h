#pragma once 
#include "Variable.h"
#include "Function.h"
#include "ExpressionList.h"
#include "Expression.h"
#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdlib>
#include <vector>

class Quad{
public:
	Quad(char*aa,char*bb,char*cc,char*dd){a=aa;b=bb;c=cc;d=dd;};
	Quad(char*aa,int bb,char*cc,char*dd){a=aa;sprintf(b,"%d",bb);c=cc;d=dd;};
	~Quad(){}
	char* a;
	char* b;
	char* c;
	char* d;
	int intA(){return atoi(a);}
	int intB(){return atoi(b);}
	int intC(){return atoi(c);}
	int intD(){return atoi(d);}
};

class SemanticAnalyser{
public:
	SemanticAnalyser(){}
	~SemanticAnalyser(){}
	bool AddIntVar(char* name);
	bool AddArrayVar(char* name,int size);
	bool AddIntConst(char* name);
	bool AddArrayConst(char* name,int size);
	bool AddIntFunction(char* name);
	bool AddArrayFunction(char* name,int size);
	bool AddIntArgument(char* name);
	bool AddArrayArgument(char* name,int size);
	int GetEntryType(char* name);
	VarType GetValueType(char* name);
	
	void OpenBlock();
	void CloseBlock();
	void PrintTable();
	int MatchAssignTypes(char* n,Expression* ex);
	int MatchExprType(VarType vt,Expression* ex);
	int CheckFunctionCall(char* n,ExpressionList* el);
	Variable* GetVariableByName(char* n);
	Function* GetFunctionByName(char* n);
private:
	std::list<TableEntry*> table;
	std::vector<Variable*> pendingArgs;
	std::vector<Variable*>* getArgList(char* name);
	bool isIdentifierAllowed(char* n,int t);
	void addMarker();
	Function* getLastFunction();
	void addToTable(TableEntry* entry);
	void eraseTillMarker();
};
