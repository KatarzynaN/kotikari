#include "Program.h"
#include "Function.h"

int UnaryEx::Compile(Function* fun){
	ofstream* output=program->output;
	int* position=0;
	int ref=0;
	if(!fun){
		ref=3;
		position=program->memPointer;
	}else{
		ref=6;
		position=fun->memPointer;
	}
	int ret=0;
	
	int add=expression->Compile(fun);
	*position+=4;
	ret=*position;
	(*output)<<"//Unary Expression:\n";
	(*output)<<"        R7=R7-4;\n";
	(*output)<<"        R5=P(R"<<ref<<"-"<<add<<");\n";
	if(operatorNum==0){
		
		(*output)<<"        R5=-R5;\n";
	}else{
		(*output)<<"        R5=!R5;\n";
	}
	(*output)<<"        P(R7)=R5;\n";
	return ret;
}
