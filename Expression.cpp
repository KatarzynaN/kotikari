#include "Program.h"
#include "Function.h"

int Expression::Compile(Function* fun){
	ofstream* output=program->output;
	int ret=0;
	int* position=0;
//	int ref=0;
	ref=0;
	if(!fun){
		ref=3;
		position=program->memPointer;
	}else{
		ref=6;
		position=fun->memPointer;
	}
	
	
	(*output)<<"//Expression\n";
	switch(variant){
		case 0:{
		//Number
			*position+=4;
			ret=*position;
			(*output)<<"        R7=R7-4;\n";
			(*output)<<"        P(R7)="<<number<<";//position: \n";
			
		break;
		}
		case 1:{
		//Variable
		if(!variable->position){
		//Is Int
			ref=variable->variable->ref;
			ret=variable->variable->address;
		}else{
		//Is ArrayVar
			(*output)<<"//ArrayVar\n";	
			int add=variable->position->Compile(fun);
			*position+=4;
			ret=*position;
			(*output)<<"        R7=R7-4;\n";
			(*output)<<"        R5=R"<<variable->variable->ref<<"-"<<variable->variable->address<<";\n";
			(*output)<<"        R2=P(R"<<ref<<"-"<<add<<");\n";
			(*output)<<"        R2=R2*4;\n";
			(*output)<<"        R5=R5+R2;\n";
			(*output)<<"        R5=P(R5);\n";
			(*output)<<"        P(R7)=R5;\n";
		}
			
			
			
		break;
		}
		case 2:{
		//FunctionCall
			ret=functionCall->Compile(fun);
		break;
		}	
		case 3:{
		//Assignment
			if(assignment->isExpression){
				//Is Expression
				
				if(assignment->expression->type.type==0){
					//Is int
					int add=assignment->expression->Compile(fun);
					
					if(!assignment->variable->position){
						int dest=assignment->variable->variable->address;
						(*output)<<"        R5=P(R"<<ref<<"-"<<add<<");\n";
						(*output)<<"        P(R"<<assignment->variable->variable->ref<<"-"<<dest<<")=R5;\n";					
					}else{
						int add1=assignment->variable->position->Compile(fun);
						(*output)<<"        R5=R"<<assignment->variable->variable->ref<<"-"<<assignment->variable->variable->address<<";\n";
						(*output)<<"        R2=P(R"<<ref<<"-"<<add1<<");\n";
						(*output)<<"        R2=R2*4;\n";
						(*output)<<"        R5=R5+R2;\n";
						(*output)<<"        R2=P(R"<<ref<<"-"<<add<<");\n";
						(*output)<<"        P(R5)=R2;\n";
					}
					
					ret=add;
			
				}else{
					//Is array

					int add=assignment->expression->Compile(fun);
					int dest=assignment->variable->variable->address;
					int aSize=assignment->expression->type.size;
					for(int i=0;i<aSize;i++){
						(*output)<<"        R5=P(R"<<ref<<"-"<<(add-4*i)<<");\n";
						(*output)<<"        P(R"<<assignment->variable->variable->ref<<"-"<<(dest-4*i)<<")=R5;\n";
					}
					ret=add;
				}
			}else{
			//Is ArrayConst
				int aSize=type.size;
				int dest=assignment->variable->variable->address;
				for(int i=0;i<aSize;i++){
					int add=assignment->arrayConst->expressions->expressions[i]->Compile(fun);
					(*output)<<"        R5=P(R"<<ref<<"-"<<add<<");\n";
					(*output)<<"        P(R"<<assignment->variable->variable->ref<<"-"<<(dest-4*i)<<")=R5;\n";
				}
				ret=dest;
			}
		break;
		}
		case 4:{
		//Unary
			ret=unaryEx->Compile(fun);
		break;
		}
		case 5:{
		//Binary
			ret=binaryEx->Compile(fun);
		break;
		}

	}
	return ret;

}
