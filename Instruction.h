#pragma once
#include "IfCondition.h"
#include "ForLoop.h"
#include "PrintInstruction.h"

class Instruction{
public:
	Instruction(){}
	~Instruction(){}
	void Compile(Function* fun);
	int type;
	IfCondition* condition;
	ForLoop* forLoop;
	Expression* expression;
	DeclarationAssignment* declaration;
	PrintInstruction* printInstruction;
	Program* program;
	
};
