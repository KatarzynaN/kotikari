#include "Program.h"
#include "Function.h"

void DeclarationAssignment::Compile(Function* fun){
	ofstream* output=program->output;
	int* position=0;
	if(!fun){
		position=program->memPointer;
	}else{
		position=fun->memPointer;
	}
	
	(*output)<<"//DeclarationAssignment: "<<variable->name<<"\n";

	if(hasValue){
		//Has value
		if(isExpression){
			//Is Expression
			if(expression->type.type==0){
				//Is int
				int add=expression->Compile(fun);
				*position+=4;
				variable->address=*position;
				if(!fun){
					variable->ref=3;
				}else{
					variable->ref=6;
				}				
				
				(*output)<<"        R7=R7-4;\n";
				(*output)<<"        R5=P(R"<<variable->ref<<"-"<<add<<");\n";
				(*output)<<"        P(R7)=R5;\n";
			
			}else{
				//Is array
				
				
				int add=expression->Compile(fun);
				int aSize=type.size;
				*position+=(4*aSize);
				variable->address=*position;
				if(!fun){
					variable->ref=3;
				}else{
					variable->ref=6;
				}
				(*output)<<"        R7=R7-"<<(4*aSize)<<";\n";
				(*output)<<"        R0=R7;\n";
				for(int i=0;i<aSize;i++){
					
					(*output)<<"        R5=P(R"<<variable->ref<<"-"<<(add-4*i)<<");\n";
					(*output)<<"        P(R0+"<<(4*i)<<")=R5;\n";
				}
		
			}
		}else{


		//-------------------------------------------------------------UNSAFE WHEN PRINT PRESENT IN INNER EXPRESSION!!!

		//Is ArrayConst
			int aSize=type.size;
			std::deque<int> addresses;
			
			for(int i=0;i<aSize;i++){
				addresses.push_back(arrayConst->expressions->expressions[i]->Compile(fun));
				
			}			

			
			*position+=(4*aSize);
			variable->address=*position;
			if(!fun){
				variable->ref=3;
			}else{
				variable->ref=6;
			}
			
			
			(*output)<<"        R7=R7-"<<(4*aSize)<<";\n";
			(*output)<<"        R0=R7;\n";
			for(int i=0;i<aSize;i++){
				int add=addresses[i];
				(*output)<<"        R5=P(R"<<variable->ref<<"-"<<add<<");\n";
				(*output)<<"        P(R0+"<<(4*i)<<")=R5;\n";
			}
			
		}
	}else{
		//No value - initialize with 0
		if(type.type==0){
			//Is int
			*position+=4;
			variable->address=*position;
			if(!fun){
				variable->ref=3;
			}else{
				variable->ref=6;
			}
			(*output)<<"        R7=R7-4;\n";
			(*output)<<"        P(R7)=0;\n";
		}else{
			//Is array
			int aSize=type.size;
			*position+=(4*aSize);
			variable->address=*position;
			if(!fun){
				variable->ref=3;
			}else{
				variable->ref=6;
			}
			(*output)<<"        R7=R7-"<<(4*aSize)<<";\n";
			for(int i=0;i<aSize;i++){
				(*output)<<"        P(R7+"<<(4*i)<<")=0;\n";
			}
		}
	}
}
