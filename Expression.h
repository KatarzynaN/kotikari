#pragma once
#include "Variable.h"
#include "VariableEx.h"
#include "FunctionCall.h"
#include "UnaryEx.h"
#include "BinaryEx.h"
#include "AssignmentEx.h"

class Expression{
public:
	Expression(){}
	~Expression(){}
	int Compile(Function* fun);

	VarType type;

	int variant;
	
	int number;
	VariableEx* variable;
	FunctionCall* functionCall;
	AssignmentEx* assignment;
	UnaryEx* unaryEx;
	BinaryEx* binaryEx;
	Program* program;
	int ref;
};

