#pragma once
#include "StringExpression.h"

class PrintInstruction{
public:
	PrintInstruction(){}
	~PrintInstruction(){}
	std::deque<StringExpression*> strings;
	Program* program;
	void Compile(Function* fun);
};
