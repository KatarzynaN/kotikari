#include "Program.h"
#include "InstructionBlock.h"

void InstructionBlock::Compile(Function* fun){
	for(int i=0;i<instructions.size();i++){
		instructions[i]->Compile(fun);
	}
}
