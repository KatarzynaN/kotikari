#pragma once
#include "Variable.h"
#include "Expression.h"
#include <deque>
class ExpressionList{
public:
	ExpressionList(){size=0;}
	ExpressionList(int s){size=s;}
	~ExpressionList(){}
	int size;
	std::deque<VarType> typeVec;
	std::deque<Expression*> expressions;
	Program* program;
	//Expressions
};
