#include "Program.h"
#include "StringExpression.h"
#include <string>
#include <cstring>

void StringExpression::Compile(Function* fun){
	ofstream* output=program->output;
	int* position=program->memPointer;
	int* label=program->label;
	int* statPos=program->statPos;
	int* statCount=program->statCount;
	int add;

	(*output)<<"//StringExpression"<<"\n";
	if(!isText){
		text="\"%i\"";
		add=expression->Compile(fun);
		
	}
	int textLen = strlen(text)-1;
	*statPos-=textLen;
	(*output)<<"STAT("<<*statCount<<")\n";
	(*output)<<"    STR(0x"<<std::hex<<*statPos<<std::dec<<","<< text <<");\n";
	(*output)<<"CODE("<<(*statCount)++<<")\n";
	
	(*output)<<"        R1=0x"<<std::hex<<*statPos<<std::dec<<";\n";
	if(!isText){
		(*output)<<"        R2=P(R"<<expression->ref<<"-"<<add<<");\n";
	}
	(*output)<<"        R0="<<*label<<";\n";
	(*output)<<"        GT(putf_); \n";
	(*output)<<"L "<<(*label)++<<":\n";

}
