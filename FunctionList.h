#pragma once
class Function;

class FunctionList{
public:
	FunctionList(){}
	~FunctionList(){}
	void Compile();

	std::deque<Function*> functions;
	Program* program;
	
};
