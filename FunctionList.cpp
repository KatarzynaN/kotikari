#include "Program.h"
#include "Function.h"
#include <string>
#include <cstring>

void FunctionList::Compile(){
	bool found=false;
	for(int i=0;i<functions.size();i++){
		functions[i]->Compile();
		if(strcmp(functions[i]->name,"main")==0){
			found=true;
			break;
		}
	}
	if(!found){
		printf("ERROR: Main function not found!");
		ofstream* output=program->output;
		(*output)<<"L 1:\n";
	}
}
