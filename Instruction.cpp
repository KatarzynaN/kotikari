#include "Program.h"
#include "Instruction.h"

void Instruction::Compile(Function* fun){
	ofstream* output=program->output;
	int* position=program->memPointer;
	
	switch(type){
		case 0:{
		//IfCond
		
		(*output)<<"//Instruction: IfCond\n";
			condition->Compile(fun);
		break;
		}
		case 1:{
		//ForLoop
		
		(*output)<<"//Instruction: ForLoop\n";
			forLoop->Compile(fun);
		break;
		}
		case 2:{//Expression
		if(!expression)
			printf("Null pointer 2!\n");
		else{
			(*output)<<"//Instruction: Expression\n";
			expression->Compile(fun);
			
		}
		
			
		break;
		}
		case 3:{
		//Declaration
			if(!declaration)
				printf("Null pointer 3!\n");
			else{
				(*output)<<"//Instruction: Declaration\n";
				declaration->Compile(fun);
			}
		break;
		}
		case 4:{
		//Print
			if(!printInstruction)
				printf("Null pointer 4!\n");
			else{
				(*output)<<"//Instruction: Print\n";
				printInstruction->Compile(fun);
			}
		

		break;
		}
	}
}
