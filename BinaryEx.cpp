#include "Program.h"
#include "Function.h"

int BinaryEx::Compile(Function* fun){
	ofstream* output=program->output;
	int* position=0;
	int ref=0;
	if(!fun){
		ref=3;
		position=program->memPointer;
	}else{
		ref=6;
		position=fun->memPointer;
	}
	int ret=0;
	int add1=expression1->Compile(fun);
	int add2=expression2->Compile(fun);

	*position+=4;
	ret=*position;
	(*output)<<"//Binary Expression:\n";
	(*output)<<"        R7=R7-4;\n";
	(*output)<<"        R5=P(R"<<ref<<"-"<<add1<<");\n";
	(*output)<<"        R2=P(R"<<ref<<"-"<<add2<<");\n";
	switch(operatorNum){
		case 2:{
			(*output)<<"        R5=R5||R2;\n";
		break;
		}
		case 3:{
			(*output)<<"        R5=R5&&R2;\n";
		break;
		}
		case 4:{
			(*output)<<"        R5=R5<R2;\n";
		break;
		}
		case 5:{
			(*output)<<"        R5=R5>R2;\n";
		break;
		}
		case 6:{
			(*output)<<"        R5=R5==R2;\n";
		break;
		}
		case 7:{
			(*output)<<"        R5=R5+R2;\n";
		break;
		}
		case 8:{
			(*output)<<"        R5=R5-R2;\n";
		break;
		}
		case 9:{
			(*output)<<"        R5=R5*R2;\n";
		break;
		}
		case 10:{
			(*output)<<"        R5=R5/R2;\n";
		break;
		}
		case 11:{
			(*output)<<"        R5=R5%R2;\n";
		break;
		}
	}
	(*output)<<"        P(R7)=R5;\n";
	return ret;

}
