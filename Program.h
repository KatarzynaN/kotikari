#pragma once
class SemanticAnalyser;
#include <iostream>
#include <fstream>

#include "VarBlock.h"
#include "FunctionList.h"

using namespace std;

class Program{
public:
	Program(){memPointer=new int;(*memPointer)=0;label=new int;(*label)=3; statCount=new int;(*statCount)=0; statPos=new int;(*statPos)=0x12000;}
	~Program(){}
	void Compile();

	SemanticAnalyser* analyser;
	VarBlock*	constBlock;
	VarBlock*	varBlock;
	FunctionList*	functions;
	ofstream* output;
	int* memPointer;
	int* label;
	int* statCount;
	int* statPos;
};
