#pragma once
class Expression;
class Function;
#include "ArrayConst.h"

class DeclarationAssignment{
public:
	DeclarationAssignment(){}
	~DeclarationAssignment(){}
	void Compile(Function* fun);
	VarType type;
	char* name;
	Variable* variable;
	Expression* expression;
	ArrayConst* arrayConst;
	bool isExpression;
	bool hasValue;
	Program* program;
};
