#pragma once

class ForLoop{
public:
	ForLoop(){}
	~ForLoop(){}
	void Compile(Function* fun);
	DeclarationAssignment* counter;
	Expression* condition;
	Expression* operation;
	InstructionBlock* block;
	Program* program;
};
